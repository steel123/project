import {createRouter, createWebHistory} from "vue-router";
import HomePage from "@/pages/HomePage";

const routes = [
    {
        path: '/',
        component: HomePage
    },
    {
        path: '/shopPage',
        name: 'ShopPage',
        component: () => import('../pages/ShopPage.vue'),
    },
    {
        path: '/single-product/:id',
        name: 'SingleProductPage',
        component: () => import('../pages/SingleProductPage.vue'),
    },
    {
        path: '/card-korzina',
        name: 'cardKorzina',
        component: () => import('../pages/KozinaCardPage.vue'),
    },
    {
        path: '/card-like',
        name: 'LikeCard',
        component: () => import('../pages/LikeCardPage.vue'),
    },
]

export default createRouter({
    history: createWebHistory(),
    scrollBehavior() {
        return {top: 0}
    },
    routes
})
